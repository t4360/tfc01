let entrada_1 = 3;
let entrada_2 = 5;

let soma = entrada_1 + entrada_2;
let diminuicao = entrada_1 - entrada_2;
let multiplicacao = entrada_1 * entrada_2;
let divisao = entrada_1 / entrada_2;

let resposta = `Para os valores de entrada 3 e 5 temos que: 

3 + 5 = ${soma};
3 - 5 = ${diminuicao};
3 * 5 = ${multiplicacao};
3 / 5 = ${divisao};

`

console.log(resposta);
