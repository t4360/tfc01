let numero_aleatorio = Math.floor(Math.random() * 20 + 1);
let nome_Joao = "Joao";
let arma_Joao = "Arco e Flechas";
let nome_Pedro = "Pedro";
let arma_Pedro = "Varinha de Inverno";
let defesa_Joao = 0 * 100;
let defesa_Pedro = 1 * 100;
let forca_ataque_Joao = 20 * numero_aleatorio + 20;
let forca_ataque_Pedro = 18 * numero_aleatorio + 17;
let valor_total_Joao = forca_ataque_Pedro - defesa_Joao;
let valor_total_Pedro = forca_ataque_Joao - defesa_Pedro;
let vida_final_Joao = valor_total_Joao + 1800;
let vida_final_Pedro = valor_total_Pedro + 1800;

if (vida_final_Joao < 0) 
{
	console.log(`${nome_Joao} perdeu ${valor_total_Joao} pontos de vida no ataque de ${nome_Pedro}. 
${nome_Pedro} perdeu ${valor_total_Pedro} pontos de vida no ataque de ${nome_Joao}. 
${nome_Joao} morreu.`);
}

else if (vida_final_Pedro < 0)
{
	console.log(`${nome_Joao} perdeu ${valor_total_Joao} pontos de vida no ataque de ${nome_Pedro}. 
${nome_Pedro} perdeu ${valor_total_Pedro} pontos de vida no ataque de ${nome_Joao}. 
${nome_Pedro} morreu.`);
}

else if (vida_final_Joao < 0 && vida_final_Pedro < 0)
{
	console.log(`${nome_Joao} perdeu ${valor_total_Joao} pontos de vida no ataque de ${nome_Pedro}. 
${nome_Pedro} perdeu ${valor_total_Pedro} pontos de vida no ataque de ${nome_Joao}. 
Todos morreram.`);
}

else
{
	console.log(`${nome_Joao} perdeu ${valor_total_Joao} pontos de vida no ataque de ${nome_Pedro}. 
${nome_Pedro} perdeu ${valor_total_Pedro} pontos de vida no ataque de ${nome_Joao}. 
Todos sobreviveram.`);
}